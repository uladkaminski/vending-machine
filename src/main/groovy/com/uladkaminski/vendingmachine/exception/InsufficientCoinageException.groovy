package com.uladkaminski.vendingmachine.exception

class InsufficientCoinageException extends RuntimeException {
    InsufficientCoinageException() {
    }

    InsufficientCoinageException(String var1) {
        super(var1)
    }

    InsufficientCoinageException(String var1, Throwable var2) {
        super(var1, var2)
    }

    InsufficientCoinageException(Throwable var1) {
        super(var1)
    }
}
