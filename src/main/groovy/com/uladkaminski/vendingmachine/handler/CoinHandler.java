package com.uladkaminski.vendingmachine.handler;

import com.uladkaminski.vendingmachine.entity.Coin;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CoinHandler {

    public static List<Coin> getCoinSortedList() {
        return Stream.of(Coin.values())
                .sorted(Comparator.comparingInt(Coin::getDenomination))
                .collect(Collectors.toList());
    }
}
