package com.uladkaminski.vendingmachine.service

import com.uladkaminski.vendingmachine.entity.Coin

class CoinInventoryInfiniteService implements CoinInventoryService {
    @Override
    int receiveCoinCountForChange(Coin coin, int cents) {
        return cents / coin.denomination as Integer
    }
}
