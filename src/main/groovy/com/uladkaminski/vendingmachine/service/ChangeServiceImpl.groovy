package com.uladkaminski.vendingmachine.service

import com.uladkaminski.vendingmachine.entity.Coin
import com.uladkaminski.vendingmachine.handler.CoinHandler
import com.uladkaminski.vendingmachine.util.ValueValidator

class ChangeServiceImpl implements ChangeService {

    @Override
    Collection<Coin> getOptimalChangeFor(int cents) {
        return getChange(cents, new CoinInventoryInfiniteService())
    }

    @Override
    Collection<Coin> getChangeFor(int cents) {
        return getChange(cents, new CoinInventoryPropertiesService())
    }

    private Collection<Coin> getChange(int cents, CoinInventoryService coinInventoryService) {
        ValueValidator.checkNotNegativeValue(cents, "Cents value should be positive value or zero")

        Collection coins = []

        List<Coin> sortedCoinsType = CoinHandler.getCoinSortedList().reverse()
        sortedCoinsType.each {
            if (cents > 0) {
                int count = coinInventoryService.receiveCoinCountForChange(it, cents)
                cents -= count * it.denomination
                for (int i = 0; i < count; i++) {
                    coins << it
                }
            }
        }

        ValueValidator.checkZeroValue(cents, "There are no enough coins")
        return coins
    }
}
