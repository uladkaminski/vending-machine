package com.uladkaminski.vendingmachine.service

import com.uladkaminski.vendingmachine.entity.Coin

class CoinInventoryPropertiesService implements CoinInventoryService {
    private static final String COINS_INVENTORY_FILE = 'properties/coin-inventory.properties'

    Map<Coin, Integer> coinsCount = [:]

    CoinInventoryPropertiesService() {
        loadCoinsCount()
    }

    int receiveCoinCountForChange(Coin coin, int cents) {
        int count = Math.min(cents / coin.denomination as Integer, coinsCount[coin])
        coinsCount[coin] -= count
        updateProperties()
        return count
    }

    private void updateProperties() {
        Properties properties = new Properties()
        File propertiesFile = new File(COINS_INVENTORY_FILE)
        coinsCount.entrySet().each {
            properties.put(it.key.denomination as String, it.value as String)
        }
        properties.store(propertiesFile.newWriter(), null)
    }

    private Map<Coin, Integer> loadCoinsCount() {
        Properties properties = loadCoinsInventoryProperties()
        Coin.values().each {
            coinsCount.put(it, properties.getOrDefault(it.denomination as String, 0) as Integer)
        }

        return coinsCount
    }

    private Properties loadCoinsInventoryProperties() {
        Properties properties = new Properties()
        File propertiesFile = new File(COINS_INVENTORY_FILE)
        propertiesFile.withInputStream {
            properties.load(it)
        }
        return properties
    }
}
