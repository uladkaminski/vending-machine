package com.uladkaminski.vendingmachine.service

import com.uladkaminski.vendingmachine.entity.Coin

interface ChangeService {

    Collection<Coin> getOptimalChangeFor(int cents)

    Collection<Coin> getChangeFor(int cents)

}