package com.uladkaminski.vendingmachine.service

import com.uladkaminski.vendingmachine.entity.Coin

interface CoinInventoryService {
    int receiveCoinCountForChange(Coin coin, int cents)

}