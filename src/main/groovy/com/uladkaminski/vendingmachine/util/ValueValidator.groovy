package com.uladkaminski.vendingmachine.util

import com.uladkaminski.vendingmachine.exception.InsufficientCoinageException

class ValueValidator {
    static void checkNotNegativeValue(Integer value, String errorMessage) {
        if (value < 0) {
            throw new IllegalArgumentException(errorMessage)
        }
    }

    static void checkZeroValue(Integer value, String errorMessage) {
        if (value != 0) {
            throw new InsufficientCoinageException(errorMessage)
        }
    }
}
