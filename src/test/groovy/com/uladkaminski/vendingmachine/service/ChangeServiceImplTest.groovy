package com.uladkaminski.vendingmachine.service

import com.uladkaminski.vendingmachine.entity.Coin
import spock.lang.Specification
import spock.lang.Unroll

class ChangeServiceImplTest extends Specification {

    ChangeService sut = new ChangeServiceImpl()

    @Unroll
    def "getOptimalChangeFor returns #expectedResult when change is #initialValueCents cents"() {
        given:
        int cents = initialValueCents

        when:
        Collection<Coin> coins = sut.getOptimalChangeFor(cents)

        then:
        coins == expectedResult

        where:
        initialValueCents | expectedResult
        0                 | []
        10                | [Coin.TEN_CENTS]
        13                | [Coin.TEN_CENTS, Coin.TWO_CENTS, Coin.ONE_CENT]
    }

    @Unroll
    def "getOptimalChangeFor throw #expectedException in case #desc"() {
        given:
        int cents = initialValueCents

        when:
        sut.getOptimalChangeFor(cents)

        then:
        thrown(expectedException)

        where:
        desc                      | initialValueCents | expectedException
        "cents value less than 0" | -1                | IllegalArgumentException
    }

}
