package com.uladkaminski.vendingmachine.service

import com.uladkaminski.vendingmachine.entity.Coin
import spock.lang.Specification
import spock.lang.Unroll

class CoinInventoryInfiniteServiceTest extends Specification {

    CoinInventoryService sut = new CoinInventoryInfiniteService()

    @Unroll
    def "ReceiveCoinCountForChange should return #expectedCount of #coin for #cents"() {
        when:
        int actualCount = sut.receiveCoinCountForChange(coin, cents)

        then:
        actualCount == expectedCount


        where:
        cents | coin            | expectedCount
        1     | Coin.ONE_CENT   | 1
        9     | Coin.ONE_CENT   | 9
        6     | Coin.FIVE_CENTS | 1
        9     | Coin.TEN_CENTS  | 0
    }
}
